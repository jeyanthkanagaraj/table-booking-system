import React, { Component } from "react";
import { withStyles, Grid, Typography, Button } from "@material-ui/core";
import moment from "moment";
import { Style } from "./Booking.style";
import baseURL from "../../constants/baseURL";
import SlotsContainer from "./SlotsContainer/SlotsContainer";
import Loader from "../../components/Loader/Loader";
import CustomSnackBar from "../../components/CustomSnackBar/CustomSnackBar";

class Booking extends Component {
  state = {
    Slots: [],
    IsBooking: false,
    IsBookingSuccess: false,
    Message: "",
    IsBookingFailed: false,
  };

  //To Get all slots
  componentDidMount() {
    this.setState({
      IsLoading: true,
    });
    let headers = {
      "Content-Type": "application/json",
    };
    baseURL
      .get("/booking", { headers })
      .then((resp) => {
        console.log(resp.data);
        return this.setState({ Slots: resp.data, IsLoading: false });
      })
      .catch((err) => {
        console.log(err);
        return this.setState({
          IsLoading: true,
        });
      });
  }

  handleClick = () => {
    this.setState({
      IsBooking: true,
    });
  };

  // To Book a slot
  handleBooking = (id) => {
    console.log(id);
    this.setState({
      IsBooking: false,
    });
    let headers = {
      "Content-Type": "application/json",
    };
    let data = {
      IsBooked: true,
    };
    baseURL
      .patch(`/booking/${id}`, data, { headers })
      .then((resp) =>
        this.setState(
          {
            Slots: this.state.Slots.map((slot) => {
              console.log(resp.data._id);
              if (slot._id === resp.data._id) {
                return {
                  ...slot,
                  IsBooked: true,
                };
              }
              return slot;
            }),
          },
          this.setState({ Message: "Table Booked Successfully" }, () => {
            this.setState({
              IsBookingSuccess: true,
            });
          })
        )
      )
      .catch((err) =>
        this.setState({ Message: "Booking Failed. Please try again later" }, () => {
          this.setState({
            IsBookingFailed: true,
          });
        })
      );
  };

  //To cancel booked slot
  cancelBooking = (id) => {
    console.log(id);
    this.setState({
      IsBooking: false,
    });
    let headers = {
      "Content-Type": "application/json",
    };
    let data = {
      IsBooked: false,
    };
    baseURL
      .patch(`/booking/${id}`, data, { headers })
      .then((resp) =>
        this.setState(
          {
            Slots: this.state.Slots.map((slot) => {
              console.log(resp.data._id);
              if (slot._id === resp.data._id) {
                return {
                  ...slot,
                  IsBooked: false,
                };
              }
              return slot;
            }),
          },
          this.setState({ Message: "Booking Cancelled Successfully" }, () => {
            this.setState({
              IsBookingSuccess: true,
            });
          })
        )
      )
      .catch((err) =>
        this.setState({ Message: "Cancellation Failed. Please try again later " }, () => {
          this.setState({
            IsBookingFailed: true,
          });
        })
      );
  };

  handleClose = () => {
    this.setState({
      IsBookingSuccess: false,
      IsBookingFailed: false,
    });
  };

  render() {
    const { classes } = this.props;
    const { Slots, IsBooking, IsLoading, IsBookingSuccess, Message, IsBookingFailed } = this.state;

    console.log(Slots);
    console.log(IsBooking, IsLoading, IsBookingSuccess, IsBookingFailed);

    //To get only upcoming slots
    const upcomingSlots = this.state.Slots.filter((slot) => new Date(slot.BookingSlot) - new Date() > 0);
    console.log(upcomingSlots);

    //To get unique slot days
    const days = upcomingSlots.map((slot) => moment(slot.BookingSlot).format("MM-DD-YYYY"));
    const uniqueDays = [...new Set(days)];
    console.log(uniqueDays);

    //To get available slots
    const availableSlots = upcomingSlots.filter((slot) => !slot.IsBooked);
    console.log(availableSlots);

    return (
      <>
        {IsLoading ? (
          <Loader />
        ) : (
          <Grid container direction="column" justify="center" alignItems="center" className={classes.container}>
            <div className={classes.header}>
              <Typography variant="h3" color="primary" className={classes.headerTitle}>
                Table Booking System
              </Typography>
            </div>
            <div className={classes.content}>
              <Grid container spacing={3} direction="row" justify="center" alignItems="center">
                <Grid item lg={8} md={9} sm={11} xs={11}>
                  <div className={classes.titleCover}>
                    <Typography variant="h4" className={classes.contentTitle}>
                      {IsBooking ? (
                        <>
                          Available Slots<span>Click a slot, to book it</span>
                        </>
                      ) : (
                        <>
                          All Slots<span>Click on the Booked slot, to cancel the slot</span>
                        </>
                      )}
                    </Typography>
                    {!IsBooking && (
                      <Button variant="contained" className={classes.bookButton} onClick={this.handleClick}>
                        Book a Table
                      </Button>
                    )}
                  </div>
                  <div className={classes.slotWrapper}>
                    <SlotsContainer
                      slots={IsBooking ? availableSlots : upcomingSlots}
                      uniqueDays={uniqueDays}
                      handleBooking={this.handleBooking}
                      IsBooking={IsBooking}
                      cancelBooking={this.cancelBooking}
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
            <CustomSnackBar IsFailed={IsBookingFailed} IsBookingSuccess={IsBookingSuccess} label={Message} handleClose={this.handleClose} />
          </Grid>
        )}
      </>
    );
  }
}

export default withStyles(Style)(Booking);
