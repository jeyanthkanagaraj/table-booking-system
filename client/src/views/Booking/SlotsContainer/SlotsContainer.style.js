export const Style = (theme) => ({
  paper: {
    padding: 20,
  },
  chip: {
    color: theme.palette.secondary.main,
    border: `1px solid ${theme.palette.secondary.main}`,
    backgroundColor: theme.palette.secondary.light,
    marginRight: 5,
    marginBottom: 5,
    cursor: "pointer",
  },
  day: {
    borderBottom: `1px solid ${theme.palette.secondary.dark}`,
    color: theme.palette.primary.main,
    width: "90%",
  },
  booked: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.light,
  },
  allBooked: {
    color: theme.palette.primary.main,
    margin: "10px 20px",
  },
});
