import React from "react";
import { withStyles, Paper, Chip, Typography } from "@material-ui/core";
import { Style } from "./SlotsContainer.style";
import moment from "moment";

const SlotsContainer = ({ classes, slots, uniqueDays, handleBooking, cancelBooking, IsBooking }) => {
  console.log(slots, uniqueDays, IsBooking);
  return (
    <>
      {uniqueDays &&
        uniqueDays.map((day) => {
          const noDates = slots.filter((slot) => moment(slot.BookingSlot).format("MM-DD-YYYY") === moment(day).format("MM-DD-YYYY"));
          return (
            <>
              <Typography className={classes.day}>{moment(day).format("ddd, DD MMM")}</Typography>
              {noDates && noDates.length > 0 ? (
                <Paper className={classes.paper} elevation={0}>
                  {slots &&
                    slots
                      .filter((slot) => moment(slot.BookingSlot).format("MM-DD-YYYY") === moment(day).format("MM-DD-YYYY"))
                      .map((slot) => (
                        <>
                          {slot.IsBooked ? (
                            <Chip
                              key={slot._id}
                              className={`${classes.chip} ${classes.booked}`}
                              label={moment(slot.BookingSlot).format("hh:mm A")}
                              onClick={() => cancelBooking(slot._id)}
                            />
                          ) : (
                            <Chip
                              key={slot._id}
                              className={classes.chip}
                              disabled={!IsBooking}
                              label={moment(slot.BookingSlot).format("hh:mm A")}
                              onClick={() => handleBooking(slot._id)}
                            />
                          )}
                        </>
                      ))}
                </Paper>
              ) : (
                <Typography className={classes.allBooked}>All slots are booked for the day</Typography>
              )}
            </>
          );
        })}
    </>
  );
};

export default withStyles(Style)(SlotsContainer);
