export const Style = (theme) => ({
  container: {
    margin: "50px auto",
    width: "70%",
  },
  headerTitle: {
    textTransform: "uppercase",
    textAlign: "center",
    marginBottom: 30,
    color: theme.palette.primary.main,
  },
  header: {
    borderBottom: `1px solid ${theme.palette.secondary.dark}`,
    width: "100%",
  },
  content: {
    marginTop: 40,
    width: "80%",
    margin: "0 auto",
  },
  contentTitle: {
    color: theme.palette.primary.main,
    textAlign: "center",
    marginBottom: 20,
    flex: 1,
    "& span": {
      display: "block",
      fontSize: 10,
    },
  },
  titleCover: {
    position: "relative",
  },
  bookButton: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.light,
    position: "absolute",
    right: 0,
    top: 0,
  },

  //Media Queries

  [theme.breakpoints.down("sm")]: {
    bookButton: {
      top: "55px",
      right: "84px",
    },
    container: {
      width: "90%",
    },
    content: {
      width: "100%",
    },
    slotWrapper: { marginTop: 60 },
  },
});
