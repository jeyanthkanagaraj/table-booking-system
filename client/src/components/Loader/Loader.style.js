export const Style = (theme) => ({
  loader: {
    color: theme.palette.secondary.main,
    position: "absolute",
    top: "45%",
    left: "50%",
  },
  loaderCover: {
    margin: 0,
    padding: 0,
    position: "fixed",
    right: 0,
    top: 0,
    width: "100%",
    zIndex: 9999,
    height: "100%",
    backgroundColor: "rgba(255, 255, 255, 0.5)",
  },
});
