export const Style = (theme) => ({
  snackBarContent: {
    backgroundColor: theme.palette.primary.contrastText,
    color: theme.palette.secondary.light,
  },
  failed: {
    backgroundColor: theme.palette.secondary.error,
  },
});
