import React from "react";
import { withStyles, Snackbar, IconButton } from "@material-ui/core";
import { Style } from "./CustomSnackBar.style";
import { Close } from "@material-ui/icons";

const CustomSnackBar = ({ classes, IsBookingSuccess, IsFailed, handleClose, label }) => {
  console.log(IsBookingSuccess, label, IsFailed);
  let open = IsBookingSuccess || IsFailed;
  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}
      open={open}
      ContentProps={{ className: `${classes.snackBarContent} ${IsFailed ? classes.failed : null}` }}
      autoHideDuration={3000}
      onClose={handleClose}
      message={label}
      action={
        <>
          <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
            <Close fontSize="small" />
          </IconButton>
        </>
      }
    />
  );
};

export default withStyles(Style)(CustomSnackBar);
