import React from "react";
import "./App.css";
import { MuiThemeProvider } from "@material-ui/core/styles";
import Booking from "./views/Booking/Booking";
import { theme } from "./constants/theme";

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Booking />
    </MuiThemeProvider>
  );
}

export default App;
