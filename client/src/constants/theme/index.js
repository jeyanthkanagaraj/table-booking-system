import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#443d77",
      light: "rgba(0,0,0,0.1)",
      dark: "#424242",
      contrastText: "#4caf50",
    },
    secondary: {
      main: "#e65284",
      light: "#fff",
      dark: "rgba(0,0,0,0.3)",
      error: "#f44336",
    },
  },
});
