const express = require("express");
const router = express.Router();
const Booking = require("../models/Booking");

// To get all slots
router.get("/", (req, res) => {
  Booking.find()
    .then((resp) => {
      console.log(resp);
      res.status(200).json(resp);
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json(err);
    });
});

// To create new slots
router.post("/", (req, res) => {
  console.log(req.body);
  const { BookingSlot, IsBooked } = req.body;

  const booking = new Booking({
    BookingSlot,
    IsBooked,
  });

  booking
    .save()
    .then((resp) => {
      console.log(resp);
      res.status(201).json(resp);
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json(err);
    });
});

// To update a slot
router.patch("/:id", (req, res) => {
  Booking.updateOne({ _id: req.params.id }, { $set: req.body })
    .then((resp) => {
      console.log(resp);
      res.status(200).json({ _id: req.params.id });
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json(err);
    });
});

module.exports = router;
