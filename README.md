### Table Booking System

The application is developed using MERN stack (Mongo Atlas, Express, ReactJS and NodeJS). 

## Getting Started

```
git clone https://gitlab.com/jeyanthkanagaraj/table-booking-system.git

cd table-booking-system

npm install //To install the required libraries to start the server side application.
//Once the required libraries installed successfully, follow the next step

cd client

npm install //To install the required libraries to start the client side application

//Once the required libraries installed successfully, follow the next step
cd .. //To navigate back to application root folder

npm run dev  //As, I have added concurrently library to start both the client and server concurrently. We don't need to run it seperately in seperate CLI
```

## Usage

Once you run the final command "npm run dev". The app will automatically open the browser window and load the app.
I have added the mongoURI, which I generated from the Mongo Atlas. So, you can test the application directly, without any 
addition of lines to the code.

## Dependencies used

In Server,

- cors - "^2.8.5"
- express - "^4.17.1"
- mongoose - "^5.9.12"

In Client,

- react - "^16.13.1"
- react-dom - "^16.13.1"
- react-scripts - "3.4.1"
- @material-ui/core - "^4.9.13"
- @material-ui/icons - "^4.9.1"
- axios - "^0.19.2"
- moment -  "^2.25.3"


