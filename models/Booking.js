const mongoose = require("mongoose");

const BookingSchema = mongoose.Schema({
  BookingSlot: {
    type: Date,
    default: Date.now,
  },
  IsBooked: {
    type: Boolean,
    required: true,
  },
});

module.exports = mongoose.model("Booking", BookingSchema);
