const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

const config = require("./config/keys");

//Whitelisting specific URLs
var whitelist = ["http://localhost:3000"];
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (whitelist.indexOf(req.header("Origin")) !== -1) {
    corsOptions = { origin: true };
  } else {
    corsOptions = { origin: false };
  }
  callback(null, corsOptions);
};

//Port
const PORT = process.env.PORT || 5000;

//Routes
const bookingRouter = require("./routes/booking");

//Middlewares
app.use(cors(corsOptionsDelegate));
app.use(express.json());
app.use("/api/booking", bookingRouter);

//DB Connection
mongoose
  .connect(config.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("DB connected..."))
  .catch((err) => console.log(err));

// For Production
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));

  const path = require("path");

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

//App listening to the port
app.listen(PORT, () => console.log(`App is running in the port ${PORT}`));
